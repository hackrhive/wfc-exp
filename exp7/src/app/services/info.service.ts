import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InfoItem } from '../model/infoItem.model';

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  private serviceURL = 'https://jsonplaceholder.typicode.com/todos';

  constructor(private http: HttpClient) { }
  getItems(): Observable<InfoItem[]> {
    return this.http.get<InfoItem[]>(this.serviceURL);
  }
}
