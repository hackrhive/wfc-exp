export interface InfoItem {
    id: number;
    userId: number;
    title: string;
    completed: boolean;
}