import { Component } from '@angular/core';
import { InfoService } from './services/info.service';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { InfoItem } from './model/infoItem.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dataSource = new ItemDataSource(this.infoService);
  displayedColumns = ['id', 'userId', 'title', 'completed'];
  constructor(private infoService: InfoService) { }
  title = 'exp7';
}
export class ItemDataSource extends DataSource<any>{
  constructor(private userService: InfoService) {
    super();
  }
  connect(): Observable<InfoItem[]> {
    return this.userService.getItems();
  }
  disconnect() { }
}