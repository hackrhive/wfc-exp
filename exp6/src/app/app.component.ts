import { Component } from '@angular/core';

export interface Items {
  name: string;
  uid: string;
  id: number;
}

const sampleData: Items[] = [
  { name: 'Chirag Rawal', uid: '17BCS4132', id: 1 },
  { name: 'Kameshwar Sahoo', uid: '17BCS4121', id: 2 },
  { name: 'Risav Chaudhary', uid: '17BCS4129', id: 3 },
  { name: 'Kartik Chaturvedi', uid: '17BCS4116', id: 4 },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  displayedColumns: string[] = ['id', 'uid', 'name'];
  dataSource = sampleData;
  title = 'exp6';
}
