import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  public signupInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  constructor() { }

  ngOnInit(): void {
  }
  async onSubmit() {
    this.signupInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('username').value;
        const password = this.form.get('password').value;
        console.log('username: ' + username);
        console.log('password: ' + password);
      } catch (err) {
        this.signupInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
}
